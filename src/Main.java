import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;


public class Main
{
	public static int count = 0;
	public static List<String> cores = Arrays.asList("AZUL", "VERDE", "AMARELO", "VERMELHO");
	
	public static void main( String[] args )
	{
		String inputPaises = JOptionPane.showInputDialog("Digite o n�mero de pa�ses:");
		String inputCores = JOptionPane.showInputDialog("Digite o n�mero de cores:");
		Integer numeroPaises = 0;
		Integer numeroCores = 0;
		
		try {
			numeroPaises = Integer.parseInt( inputPaises );
			numeroCores = Integer.parseInt( inputCores );
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Dados inv�lidos, favor tentar novamente.");
			return;
		}

		Main.cores = new ArrayList<String>();
		for (int i = 0; i < numeroCores; i++) {
			Main.cores.add("COR"+(i+1));
		}
		
		List<Pais> paises = GeradorInstancias.gerarInstancias(numeroPaises);
		
		try {
			paises.get(0).setCor(Main.cores.get(0), null);
		} catch (Exception e) {
			// ignore
		}

		boolean isOk = true;
		
		String resultado = "";
		for (Pais pais : paises) {
			isOk = isOk && pais.getCor() != null;
			resultado += pais.toString() + "\n";
		}
		
		resultado = (isOk ? "SUCESSO" : "FALHA" ) + "\n" + resultado;
		JOptionPane.showMessageDialog(null, resultado);
		
	}

}