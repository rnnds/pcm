import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Pais
{
	private List<Pais> vizinhos = new ArrayList<Pais>();
	
	private String nome;
	
	private String cor;
	
	public void addVizinho( Pais pais )
	{
		if( vizinhos.indexOf(pais) == -1 ){
			vizinhos.add(pais);
			pais.addVizinho(this);
		}	
	}
	
	public Pais(String nome) {
		super();
		this.nome = nome;
	}

	public boolean equals( Object obj )
	{
		if( obj instanceof Pais ){
			return ((Pais) obj).nome.equals( nome );
		}
		return false;
	}
	
	public int hashcode()
	{
		return nome.hashCode();
	}

	public List<Pais> getVizinhos() {
		return vizinhos;
	}

	public String getNome() {
		return nome;
	}

	public String getCor() {
		return cor;
	}
	
	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("" + this.nome + " - " + this.cor +" - Vizinhos: [" );
		for( Pais pais : getVizinhos() ){
			sb.append( pais.getNome() );
			sb.append( ", " );
		}
		
		return sb.substring(0, sb.length() - 2) + "]";
	}

	public void setCor(String cor, Pais origem) {
		
		Main.count++;
		
		for (Pais vizinho : vizinhos) {
			if(vizinho.getCor() != null && vizinho.getCor().equals(cor)){
				throw new RuntimeException();
			}
		}
		this.cor = cor;
		int indiceCor = Main.cores.indexOf(cor);
		List<String> novasCores = new ArrayList<String>(Main.cores);
		novasCores.remove(indiceCor);
		
		for (Pais vizinho : vizinhos) {
			if(!vizinho.equals(origem)){
				for (String novaCor : novasCores) {
					if(vizinho.getCor() != null){
						break;
					}
					try {
						vizinho.setCor(novaCor, this);
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		}
	}
	
} 