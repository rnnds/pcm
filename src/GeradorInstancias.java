import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class GeradorInstancias 
{
	private static Random RANDOM = new Random();
	
	public static void main(String[] args) {
		gerarInstancias(100);
	}
	
	public static List<Pais> gerarInstancias( int quantidade )
	{
		List<Pais> paises = new ArrayList<Pais>(quantidade);
		
		for( int i=0 ; i < quantidade ; i++ )
		{
			Pais pais = new Pais("Pais_"+i);
			paises.add(pais);
		}

		int indexAtual=0;
		int limiteTentativas=0;
		Set<Pais> countPaises = new HashSet<Pais>(quantidade);
		
		while( countPaises.size() != quantidade && limiteTentativas < 40  )
		{
			Pais paisAtual = paises.get( indexAtual );
			
			int indexVizinho = RANDOM.nextInt(quantidade);
			Pais paisVizinho = paises.get( indexVizinho );
			
			if( paisVizinho.getVizinhos().size() < 5 && paisAtual.getVizinhos().size() < 5 
					&& paisAtual.getVizinhos().indexOf(paisVizinho) == -1 && ! paisAtual.equals(paisVizinho) )
			{
				countPaises.add(paisAtual);
				paisAtual.addVizinho(paisVizinho);
				
				limiteTentativas=0;
			}
			else {
				limiteTentativas++;
			}
			
			indexAtual = indexVizinho;
		}
		
		return paises;
	}
}
